/**
 ****************************************************************************************************
 	@file			timer.h
 	@author			Michal Szczygiel
 	$Contact:		michal.szczygiel@zeg-energetyka.pl$
 	$MCU:			STM32F205RB$
 	$IDE:			Atollic TrueSTUDIO for STM32 v9.0.1$
 	@version		v0.1
 	$Revision: 		v0.0.1$
 	@date			26.04.2018r.
 	@brief			Timery
 	@copyright		ZEG-ENERGETYKA
 ****************************************************************************************************
*/

#ifndef TIMER_H_
	#define TIMER_H_

	#define TIME_1S   				1000			/**< Czas 1 sekunda */
	#define TIME_1_5S				1500			/**< Czas 1.5 sekundy */
	#define TIME_2S   				2000			/**< Czas 2 sekundy */
	#define TIME_5S   				5000			/**< Czas 5 sekund */
	#define	TIME_TR1_TIMEOUT_TIME_BETWEEN_CHARS 10	/**< Czas 10 mili sekundy */
	#define	TIME_TR2_TIMEOUT_TIME_BETWEEN_CHARS 10	/**< Czas 10 mili sekundy */
	#define	TIME_TR3_TIMEOUT_TIME_BETWEEN_CHARS 10	/**< Czas 10 mili sekundy */
	#define TIME_BETWEEN_FRAMES		18				/**< Czas 18 mili sekundy */

	#define TIMER_OFF   -1
	#define TIMER_END   0
	#define TIMER_BUSY  1

	#define TIMER_TABLE_1MS \
		TIMER_1MS(LED1) \
		TIMER_1MS(LED2) \
		TIMER_1MS(LED3) \
		TIMER_1MS(LED4)\
		TIMER_1MS(WS2812)
	#define TIMER_1MS(arg) eTim1MS_##arg,

//------ Definicje Timerow 1ms ------------------
	enum eTim1MS {
			TIMER_TABLE_1MS
			eTim1MS_MAX
		};
	#undef TIMER_1MS


	extern volatile signed int Timer_ms[eTim1MS_MAX];

	#define TimerStart(TimerNr, Time)	Timer_ms[TimerNr] = Time
	#define TimerStop(TimerNr)			Timer_ms[TimerNr] = TIMER_OFF
	#define TimerEnded(TimerNr)			Timer_ms[TimerNr] == TIMER_END
	#define TimerStatus(TimerNr)		Timer_ms[TimerNr]
	#define TimerRead(TimerNr)			Timer_ms[TimerNr]


	void Timer_Init(void);
	void InitSystemTimer(void);
	void TimersStart(void);

#endif /* TIMER_H_ */
