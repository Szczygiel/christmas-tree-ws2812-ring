/**
 ****************************************************************************************************
 	@file			timer.c
 	@author			Michal Szczygiel
 	$Contact:		michal.szczygiel@zeg-energetyka.pl$
 	$MCU:			STM32F205RB$
 	$IDE:			Atollic TrueSTUDIO for STM32 v9.0.1$
 	@version		v0.1
 	$Revision: 		v0.0.1$
 	@date			26.04.2018r.
 	@brief			Timery
 	@copyright		ZEG-ENERGETYKA
 ****************************************************************************************************
*/

/* Includes */
#include <stdbool.h>
#include <STM32F1XX.h>
#include "../peripherals/timer.h"


/**
 * TIM1: 1ms - System Timer
 */

volatile signed int Timer_ms[eTim1MS_MAX];

void TimersStart(void)
{
	TimerStart(eTim1MS_WS2812, 100);
	TimerStart(eTim1MS_LED1, 100);

}
