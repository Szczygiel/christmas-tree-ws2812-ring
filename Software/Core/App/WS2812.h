/*
 * WS2812.h
 *
 *  Created on: 02.07.2018
 *      Author: Michal_Szczygiel
 */

#ifndef APP_WS2812_H_
#define APP_WS2812_H_

#define PIXEL_LENGHT	16
#define LEDNUMBERS 		32 + 24 + 16 + 12 + 8 + 1

#define GREEN	0x00
#define RED		0x01
#define BLUE	0x02

extern uint32_t buffer[];
extern 	uint8_t RgbTable[LEDNUMBERS][3];

void LEDTableToBuffer(uint32_t *buffer, uint8_t *RgbTable, uint8_t RgbTableLength);
void table32to16(uint32_t *buf32, uint16_t *buf16, uint16_t Length);
void RotaryShiftTable(uint8_t *buf16, uint8_t lenght);
void RotaryShiftTableRight(uint8_t *RgbTable, uint8_t lenght);
void RotaryShiftTableLeft(uint8_t *RgbTable, uint8_t lenght);
void FadeInTable(uint8_t *RgbTable, uint8_t colour, uint8_t step, uint8_t lenght);
void FadeOutTable(uint8_t *RgbTable, uint8_t colour, uint8_t step, uint8_t lenght);
void ClearPixels(uint8_t *RgbTable, uint8_t lenght);
void SetPixelColour(uint8_t *RgbTable, uint8_t ColourRed, uint8_t ColourGreen, uint8_t ColourBlue, uint8_t Pixel);
void SetOnlyOnePixelColour(uint8_t *RgbTable, uint8_t ColourRed, uint8_t ColourGreen, uint8_t ColourBlue, uint8_t Pixel, uint8_t lenght);
void setPixelHeatColor(uint8_t *RgbTable, uint8_t Pixel, uint8_t temperature);
void Fire(uint8_t Cooling, uint8_t Sparking, uint8_t SpeedDelay);

#endif /* APP_WS2812_H_ */
