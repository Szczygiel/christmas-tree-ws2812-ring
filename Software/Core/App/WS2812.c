/*
 * WS2812.c
 *
 *  Created on: 02.07.2018
 *      Author: Michal_Szczygiel
 */

#include "main.h"
#include "stdint.h"

	uint32_t buffer[] =
	{0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,	// Każdy element tabeli opisuje jedną diodę.
//* Krąg pierwszy - 32 diody LED
/* Led 1 */			0x00924924, 0x00924924, 0x00924924,											// Z 32 bitów używane jest tylko 24 bity bo tyle określa jedną diodę
/* Led 2 */			0x00924924, 0x00924924, 0x00924924,
/* Led 3 */			0x00924924, 0x00924924, 0x00924924,
/* Led 4 */			0x00924924, 0x00924924, 0x00924924,
/* Led 5 */			0x00924924, 0x00924924, 0x00924924,
/* Led 6 */			0x00924924, 0x00924924, 0x00924924,
/* Led 7 */			0x00924924, 0x00924924, 0x00924924,
/* Led 8 */			0x00924924, 0x00924924, 0x00924924,
/* Led 9 */			0x00924924, 0x00924924, 0x00924924,
/* Led 10*/			0x00924924, 0x00924924, 0x00924924,
/* Led 11*/			0x00924924, 0x00924924, 0x00924924,
/* Led 12*/			0x00924924, 0x00924924, 0x00924924,
/* Led 13*/			0x00924924, 0x00924924, 0x00924924,
/* Led 14*/			0x00924924, 0x00924924, 0x00924924,
/* Led 15*/			0x00924924, 0x00924924, 0x00924924,
/* Led 16*/			0x00924924, 0x00924924, 0x00924924,
/* Led 17*/			0x00924924, 0x00924924, 0x00924924,
/* Led 18*/			0x00924924, 0x00924924, 0x00924924,
/* Led 19*/			0x00924924, 0x00924924, 0x00924924,
/* Led 20*/			0x00924924, 0x00924924, 0x00924924,
/* Led 21*/			0x00924924, 0x00924924, 0x00924924,
/* Led 22*/			0x00924924, 0x00924924, 0x00924924,
/* Led 23*/			0x00924924, 0x00924924, 0x00924924,
/* Led 24*/			0x00924924, 0x00924924, 0x00924924,
/* Led 25*/			0x00924924, 0x00924924, 0x00924924,
/* Led 26*/			0x00924924, 0x00924924, 0x00924924,
/* Led 27*/			0x00924924, 0x00924924, 0x00924924,
/* Led 28*/			0x00924924, 0x00924924, 0x00924924,
/* Led 29*/			0x00924924, 0x00924924, 0x00924924,
/* Led 30*/			0x00924924, 0x00924924, 0x00924924,
/* Led 31*/			0x00924924, 0x00924924, 0x00924924,
/* Led 32*/			0x00924924, 0x00924924, 0x00924924,

//* Krąg drugi - 24 diody LED
/* Led 1 */			0x00924924, 0x00924924, 0x00924924,											// Z 32 bitów używane jest tylko 24 bity bo tyle określa jedną diodę
/* Led 2 */			0x00924924, 0x00924924, 0x00924924,
/* Led 3 */			0x00924924, 0x00924924, 0x00924924,
/* Led 4 */			0x00924924, 0x00924924, 0x00924924,
/* Led 5 */			0x00924924, 0x00924924, 0x00924924,
/* Led 6 */			0x00924924, 0x00924924, 0x00924924,
/* Led 7 */			0x00924924, 0x00924924, 0x00924924,
/* Led 8 */			0x00924924, 0x00924924, 0x00924924,
/* Led 9 */			0x00924924, 0x00924924, 0x00924924,
/* Led 10*/			0x00924924, 0x00924924, 0x00924924,
/* Led 11*/			0x00924924, 0x00924924, 0x00924924,
/* Led 12*/			0x00924924, 0x00924924, 0x00924924,
/* Led 13*/			0x00924924, 0x00924924, 0x00924924,
/* Led 14*/			0x00924924, 0x00924924, 0x00924924,
/* Led 15*/			0x00924924, 0x00924924, 0x00924924,
/* Led 16*/			0x00924924, 0x00924924, 0x00924924,
/* Led 17*/			0x00924924, 0x00924924, 0x00924924,
/* Led 18*/			0x00924924, 0x00924924, 0x00924924,
/* Led 19*/			0x00924924, 0x00924924, 0x00924924,
/* Led 20*/			0x00924924, 0x00924924, 0x00924924,
/* Led 21*/			0x00924924, 0x00924924, 0x00924924,
/* Led 22*/			0x00924924, 0x00924924, 0x00924924,
/* Led 23*/			0x00924924, 0x00924924, 0x00924924,
/* Led 24*/			0x00924924, 0x00924924, 0x00924924,

//* Krąg trzeci - 16 diod LED
/* Led 1 */			0x00924924, 0x00924924, 0x00924924,											// Z 32 bitów używane jest tylko 24 bity bo tyle określa jedną diodę
/* Led 2 */			0x00924924, 0x00924924, 0x00924924,
/* Led 3 */			0x00924924, 0x00924924, 0x00924924,
/* Led 4 */			0x00924924, 0x00924924, 0x00924924,
/* Led 5 */			0x00924924, 0x00924924, 0x00924924,
/* Led 6 */			0x00924924, 0x00924924, 0x00924924,
/* Led 7 */			0x00924924, 0x00924924, 0x00924924,
/* Led 8 */			0x00924924, 0x00924924, 0x00924924,
/* Led 9 */			0x00924924, 0x00924924, 0x00924924,
/* Led 10*/			0x00924924, 0x00924924, 0x00924924,
/* Led 11*/			0x00924924, 0x00924924, 0x00924924,
/* Led 12*/			0x00924924, 0x00924924, 0x00924924,
/* Led 13*/			0x00924924, 0x00924924, 0x00924924,
/* Led 14*/			0x00924924, 0x00924924, 0x00924924,
/* Led 15*/			0x00924924, 0x00924924, 0x00924924,
/* Led 16*/			0x00924924, 0x00924924, 0x00924924,

//* Krąg czwarty - 12 diod LED
/* Led 1 */			0x00924924, 0x00924924, 0x00924924,											// Z 32 bitów używane jest tylko 24 bity bo tyle określa jedną diodę
/* Led 2 */			0x00924924, 0x00924924, 0x00924924,
/* Led 3 */			0x00924924, 0x00924924, 0x00924924,
/* Led 4 */			0x00924924, 0x00924924, 0x00924924,
/* Led 5 */			0x00924924, 0x00924924, 0x00924924,
/* Led 6 */			0x00924924, 0x00924924, 0x00924924,
/* Led 7 */			0x00924924, 0x00924924, 0x00924924,
/* Led 8 */			0x00924924, 0x00924924, 0x00924924,
/* Led 9 */			0x00924924, 0x00924924, 0x00924924,
/* Led 10*/			0x00924924, 0x00924924, 0x00924924,
/* Led 11*/			0x00924924, 0x00924924, 0x00924924,
/* Led 12*/			0x00924924, 0x00924924, 0x00924924,

//* Krąg piąty - 8 diod LED
/* Led 1 */			0x00924924, 0x00924924, 0x00924924,											// Z 32 bitów używane jest tylko 24 bity bo tyle określa jedną diodę
/* Led 2 */			0x00924924, 0x00924924, 0x00924924,
/* Led 3 */			0x00924924, 0x00924924, 0x00924924,
/* Led 4 */			0x00924924, 0x00924924, 0x00924924,
/* Led 5 */			0x00924924, 0x00924924, 0x00924924,
/* Led 6 */			0x00924924, 0x00924924, 0x00924924,
/* Led 7 */			0x00924924, 0x00924924, 0x00924924,
/* Led 8 */			0x00924924, 0x00924924, 0x00924924,

//* Krąg szósty - 1 diody LED - tzw gwiazda
/* Led 1*/			0x00924924, 0x00924924, 0x00924924,

			0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000};

uint8_t RgbTable[LEDNUMBERS][3] =
{
 // GREEN, RED,  BLUE
	{0x00, 0x00, 0x00},		//Led 1
	{0x00, 0x00, 0x00},		//Led 2
	{0x00, 0x00, 0x00},		//Led 3
	{0x00, 0x00, 0x00},		//Led 4
	{0x00, 0x00, 0x00},		//Led 5
	{0x00, 0x00, 0x00},		//Led 6
	{0x00, 0x00, 0x00},		//Led 7
	{0x00, 0x00, 0x00},		//Led 8
	{0x00, 0x00, 0x00},		//Led 9
	{0x00, 0x00, 0x00},		//Led 10
	{0x00, 0x00, 0x00},		//Led 11
	{0x00, 0x00, 0x00},		//Led 12
	{0x00, 0x00, 0x00},		//Led 13
	{0x00, 0x00, 0x00},		//Led 14
	{0x00, 0x00, 0x00},		//Led 15
	{0x00, 0x00, 0x00},		//Led 16
	{0x00, 0x00, 0x00},		//Led 17
	{0x00, 0x00, 0x00},		//Led 18
	{0x00, 0x00, 0x00},		//Led 19
	{0x00, 0x00, 0x00},		//Led 20
	{0x00, 0x00, 0x00},		//Led 21
	{0x00, 0x00, 0x00},		//Led 22
	{0x00, 0x00, 0x00},		//Led 23
	{0x00, 0x00, 0x00},		//Led 24
	{0x00, 0x00, 0x00},		//Led 25
	{0x00, 0x00, 0x00},		//Led 26
	{0x00, 0x00, 0x00},		//Led 27
	{0x00, 0x00, 0x00},		//Led 28
	{0x00, 0x00, 0x00},		//Led 29
	{0x00, 0x00, 0x00},		//Led 30
	{0x00, 0x00, 0x00},		//Led 31
	{0x00, 0x00, 0x00},		//Led 32

	{0x00, 0x00, 0x00},		//Led 1
	{0x00, 0x00, 0x00},		//Led 2
	{0x00, 0x00, 0x00},		//Led 3
	{0x00, 0x00, 0x00},		//Led 4
	{0x00, 0x00, 0x00},		//Led 5
	{0x00, 0x00, 0x00},		//Led 6
	{0x00, 0x00, 0x00},		//Led 7
	{0x00, 0x00, 0x00},		//Led 8
	{0x00, 0x00, 0x00},		//Led 9
	{0x00, 0x00, 0x00},		//Led 10
	{0x00, 0x00, 0x00},		//Led 11
	{0x00, 0x00, 0x00},		//Led 12
	{0x00, 0x00, 0x00},		//Led 13
	{0x00, 0x00, 0x00},		//Led 14
	{0x00, 0x00, 0x00},		//Led 15
	{0x00, 0x00, 0x00},		//Led 16
	{0x00, 0x00, 0x00},		//Led 17
	{0x00, 0x00, 0x00},		//Led 18
	{0x00, 0x00, 0x00},		//Led 19
	{0x00, 0x00, 0x00},		//Led 20
	{0x00, 0x00, 0x00},		//Led 21
	{0x00, 0x00, 0x00},		//Led 22
	{0x00, 0x00, 0x00},		//Led 23
	{0x00, 0x00, 0x00},		//Led 24

	{0x00, 0x00, 0x00},		//Led 1
	{0x00, 0x00, 0x00},		//Led 2
	{0x00, 0x00, 0x00},		//Led 3
	{0x00, 0x00, 0x00},		//Led 4
	{0x00, 0x00, 0x00},		//Led 5
	{0x00, 0x00, 0x00},		//Led 6
	{0x00, 0x00, 0x00},		//Led 7
	{0x00, 0x00, 0x00},		//Led 8
	{0x00, 0x00, 0x00},		//Led 9
	{0x00, 0x00, 0x00},		//Led 10
	{0x00, 0x00, 0x00},		//Led 11
	{0x00, 0x00, 0x00},		//Led 12
	{0x00, 0x00, 0x00},		//Led 13
	{0x00, 0x00, 0x00},		//Led 14
	{0x00, 0x00, 0x00},		//Led 15
	{0x00, 0x00, 0x00},		//Led 16

	{0x00, 0x00, 0x00},		//Led 1
	{0x00, 0x00, 0x00},		//Led 2
	{0x00, 0x00, 0x00},		//Led 3
	{0x00, 0x00, 0x00},		//Led 4
	{0x00, 0x00, 0x00},		//Led 5
	{0x00, 0x00, 0x00},		//Led 6
	{0x00, 0x00, 0x00},		//Led 7
	{0x00, 0x00, 0x00},		//Led 8
	{0x00, 0x00, 0x00},		//Led 9
	{0x00, 0x00, 0x00},		//Led 10
	{0x00, 0x00, 0x00},		//Led 11
	{0x00, 0x00, 0x00},		//Led 12

	{0x00, 0x00, 0x00},		//Led 1
	{0x00, 0x00, 0x00},		//Led 2
	{0x00, 0x00, 0x00},		//Led 3
	{0x00, 0x00, 0x00},		//Led 4
	{0x00, 0x00, 0x00},		//Led 5
	{0x00, 0x00, 0x00},		//Led 6
	{0x00, 0x00, 0x00},		//Led 7
	{0x00, 0x00, 0x00},		//Led 8

	{0x00, 0x00, 0x00},		//Led 1
};

void LEDTableToBuffer(uint32_t *buffer, uint8_t *RgbTable, uint8_t RgbTableLength)
{
	uint32_t i;
	uint32_t bitLiczby;
	uint8_t bitKoloru;

	for(i=0; i<RgbTableLength*3; i++)
	{
		bitKoloru = 1;
		bitLiczby = 2;
		while(bitKoloru)
		{
			if(*RgbTable & bitKoloru)
			{
				*buffer |= bitLiczby; /*<ustawianie bitu */
			}
			else
			{
				*buffer &= ~bitLiczby; /*<zerowanie bitu */
			}
			bitLiczby <<= 3;
			bitKoloru <<= 1;
		}
		buffer++;
		RgbTable++;
	}
}

void table32to16(uint32_t *buf32, uint16_t *buf16, uint16_t Length)
{
	uint16_t i, j;

	for(i=0, j=0; i<Length*3; i+=2, j+=3)
	{
		buf16[j] = (buf32[i]>>8)&0x0000ffff;
		buf16[j+1] = ((buf32[i]&0x000000ff)<<8) + ((buf32[i+1]>>16)&0x000000ff);
		buf16[j+2] = buf32[i+1]&0x0000ffff;
	}
}

void RotaryShiftTableLeft(uint8_t *RgbTable, uint8_t lenght)
{
	uint8_t j, tmp[3];

	tmp[0] = *(RgbTable+0);
	tmp[1] = *(RgbTable+1);
	tmp[2] = *(RgbTable+2);

	for(j=0; j<lenght-1; j++)
	{
		*(RgbTable+0) = *(RgbTable+3+0);
		*(RgbTable+1) = *(RgbTable+3+1);
		*(RgbTable+2) = *(RgbTable+3+2);
		RgbTable += 3;
	}
	*(RgbTable+0) = tmp[0];
	*(RgbTable+1) = tmp[1];
	*(RgbTable+2) = tmp[2];
}

void RotaryShiftTableRight(uint8_t *RgbTable, uint8_t lenght)
{
	uint8_t j, tmp[3];

	RgbTable += (lenght-1)*3;

	tmp[0] = *(RgbTable+0);
	tmp[1] = *(RgbTable+1);
	tmp[2] = *(RgbTable+2);

	for(j=lenght-1; j>0; j--)
	{
		*(RgbTable+0) = *(RgbTable-3+0);
		*(RgbTable+1) = *(RgbTable-3+1);
		*(RgbTable+2) = *(RgbTable-3+2);
		RgbTable -= 3;
	}
	*(RgbTable+0) = tmp[0];
	*(RgbTable+1) = tmp[1];
	*(RgbTable+2) = tmp[2];
}

void FadeInTable(uint8_t *RgbTable, uint8_t Color, uint8_t step, uint8_t lenght)
{
	uint8_t j;

	for(j=0; j<lenght; j++)
	{
		if(*(RgbTable+Color) != 0xff)
			*(RgbTable+Color) += step;
		RgbTable += 3;
	}
}


void FadeOutTable(uint8_t *RgbTable, uint8_t Color, uint8_t step, uint8_t lenght)
{
	uint8_t j;

	for(j=0; j<lenght; j++)
	{
		if(*(RgbTable+Color) != 0x00)
			*(RgbTable+Color) -= step;
		RgbTable += 3;
	}
}

void ClearPixels(uint8_t *RgbTable, uint8_t lenght)
{
	uint8_t j;

	for(j=0; j<lenght*3; j++)
	{
		*RgbTable = 0x00;
		RgbTable++;
	}

}

void SetPixelColor(uint8_t *RgbTable, uint8_t ColorRed, uint8_t ColorGreen, uint8_t ColorBlue, uint8_t Pixel)
{
	*(RgbTable + (Pixel-1)*3 + 0) = ColorGreen;
	*(RgbTable + (Pixel-1)*3 + 1) = ColorRed;
	*(RgbTable + (Pixel-1)*3 + 2) = ColorBlue;
}

void SetOnlyOnePixelColor(uint8_t *RgbTable, uint8_t ColorRed, uint8_t ColorGreen, uint8_t ColorBlue, uint8_t Pixel, uint8_t lenght)
{
	uint8_t j;

	for(j=0; j<lenght; j++)
	{
		if(j == Pixel-1)
		{
			*(RgbTable + 0) = ColorGreen;
			*(RgbTable + 1) = ColorRed;
			*(RgbTable + 2) = ColorBlue;
		}
		else
		{
			*(RgbTable + 0) = 0x00;
			*(RgbTable + 1) = 0x00;
			*(RgbTable + 2) = 0x00;
		}
		RgbTable += 3;
	}
}

void Fire(uint8_t Cooling, uint8_t Sparking, uint8_t SpeedDelay)
{
	static uint8_t heat[PIXEL_LENGHT];
	int cooldown;

	// Step 1.  Cool down every cell a little
	for( int i = 0; i < PIXEL_LENGHT; i++)
	{
		cooldown = random()% ((Cooling * 10 / PIXEL_LENGHT) + 2);

		if(cooldown>heat[i])
		{
			heat[i]=0;
		}
		else
		{
			heat[i]=heat[i]-cooldown;
		}
	}

	// Step 2.  Heat from each cell drifts 'up' and diffuses a little
	for( int k= PIXEL_LENGHT - 1; k >= 2; k--)
	{
		heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2]) / 3;
	}

	// Step 3.  Randomly ignite new 'sparks' near the bottom
	if( random()%255 < Sparking )
	{
		int y = random()%7;
		heat[y] = heat[y] + 160+random()%(255-160);
		//heat[y] = random(160,255);
	}

	// Step 4.  Convert heat to LED colors
	for( int j = 0; j < PIXEL_LENGHT; j++)
	{
		setPixelHeatColor(&RgbTable[0][0], j, heat[j] );
	}

	czas = SpeedDelay;
}

void setPixelHeatColor(uint8_t *RgbTable, uint8_t Pixel, uint8_t temperature)
{
	// Scale 'heat' down from 0-255 to 0-191
	uint32_t t192 = temperature*191/255.0;

	// calculate ramp up from
	uint8_t heatramp = t192 & 0x3F; // 0..63
	heatramp <<= 2; // scale up to 0..252

	// figure out which third of the spectrum we're in:
	if( t192 > 0x80)                     // hottest
	{
		SetPixelColor(RgbTable, 255, 255, heatramp, Pixel);
	}
	else if( t192 > 0x40 )              // middle
	{
		SetPixelColor(RgbTable, 255, heatramp, 0, Pixel);
	}
	else                                // coolest
	{
		SetPixelColor(RgbTable, heatramp, 0, 0, Pixel);
	}
}

void PoliceRoosterRedBlue(uint8_t *LedTable)
{
	switch(stan)
	{
		case 0:
			czas = 100;
			stan++;
			for(i=0; i< PIXEL_LENGHT/2; i++)
				SetPixelColor(LedTable, 0xff, 0x00, 0x00, i+1);
			for(i=PIXEL_LENGHT/2; i< PIXEL_LENGHT; i++)
				SetPixelColor(LedTable, 0x00, 0x00, 0xff, i+1);
			break;
		case 1:
			czas = 100;
			stan++;
			ClearPixels(LedTable, PIXEL_LENGHT);
			break;
		case 2:
			czas = 100;
			stan++;
			for(i=0; i< PIXEL_LENGHT/2; i++)
				SetPixelColor(LedTable, 0xff, 0x00, 0x00, i+1);
			for(i=PIXEL_LENGHT/2; i< PIXEL_LENGHT; i++)
				SetPixelColor(LedTable, 0x00, 0x00, 0xff, i+1);
			break;
		case 3:
			czas = 100;
			stan++;
			ClearPixels(LedTable, PIXEL_LENGHT);
			break;
		case 4:
			czas = 100;
			stan++;
			for(i=0; i< PIXEL_LENGHT/2; i++)
				SetPixelColor(LedTable, 0xff, 0x00, 0x00, i+1);
			for(i=PIXEL_LENGHT/2; i< PIXEL_LENGHT; i++)
				SetPixelColor(LedTable, 0x00, 0x00, 0xff, i+1);
			break;
		case 5:
			czas = 500;
			stan++;
			ClearPixels(LedTable, PIXEL_LENGHT);
			break;
		case 6:
			czas = 100;
			stan++;
			for(i=0; i< PIXEL_LENGHT/2; i++)
				SetPixelColor(LedTable, 0x00, 0x00, 0xff, i+1);
			for(i=PIXEL_LENGHT/2; i< PIXEL_LENGHT; i++)
				SetPixelColor(LedTable, 0xff, 0x00, 0x00, i+1);
			break;
		case 7:
			czas = 100;
			stan++;
			ClearPixels(LedTable, PIXEL_LENGHT);
			break;
		case 8:
			czas = 100;
			stan++;
			for(i=0; i< PIXEL_LENGHT/2; i++)
				SetPixelColor(LedTable, 0x00, 0x00, 0xff, i+1);
			for(i=PIXEL_LENGHT/2; i< PIXEL_LENGHT; i++)
				SetPixelColor(LedTable, 0xff, 0x00, 0x00, i+1);
			break;
		case 9:
			czas = 100;
			stan++;
			ClearPixels(LedTable, PIXEL_LENGHT);
			break;
		case 10:
			czas = 100;
			stan++;
			for(i=0; i< PIXEL_LENGHT/2; i++)
				SetPixelColor(LedTable, 0x00, 0x00, 0xff, i+1);
			for(i=PIXEL_LENGHT/2; i< PIXEL_LENGHT; i++)
				SetPixelColor(LedTable, 0xff, 0x00, 0x00, i+1);
			break;
		case 11:
			czas = 500;
			stan = 0;
			ClearPixels(LedTable, PIXEL_LENGHT);
			break;
		default:
			czas = 50;
			stan = 0;
			break;
	}
}

void FireRooster(uint8_t *LedTable)
{
	switch(stan)
	{
		case 0:
			czas = 100;
			stan++;
			for(i=0; i< PIXEL_LENGHT; i++)
				SetPixelColor(LedTable, 0xff, 0x00, 0x00, i+1);
			break;
		case 1:
			czas = 100;
			stan++;
			ClearPixels(LedTable, PIXEL_LENGHT);
			break;
		case 2:
			czas = 100;
			stan++;
			for(i=0; i< PIXEL_LENGHT; i++)
				SetPixelColor(LedTable, 0xff, 0x00, 0x00, i+1);
			break;
		case 3:
			czas = 100;
			stan++;
			ClearPixels(LedTable, PIXEL_LENGHT);
			break;
		case 4:
			czas = 100;
			stan++;
			for(i=0; i< PIXEL_LENGHT; i++)
				SetPixelColor(LedTable, 0xff, 0x00, 0x00, i+1);
			break;
		case 5:
			czas = 500;
			stan = 0;
			ClearPixels(LedTable, PIXEL_LENGHT);
			break;
		default:
			czas = 50;
			stan = 0;
			break;
	}
}

void RotarySnake(uint8_t *LedTable, uint8_t red, uint8_t green, uint8_t blue)
{
	static uint8_t position = 1;

	SetPixelColor(LedTable, red, green, blue, position);
	SetPixelColor(LedTable, 0x00, 0x00, 0x00, position-5);
	if(j<6)
	{
		SetPixelColor(LedTable, 0x00, 0x00, 0x00, PIXEL_LENGHT-5+position);
	}

	position++;
	if(position == PIXEL_LENGHT+1)
		position=1;
}

void Speedometer(uint8_t *LedTable, uint8_t red, uint8_t green, uint8_t blue)
{
	if(!stan)
	{
		SetPixelColor(LedTable, red, green, blue, j);
		j++;
		if(j == PIXEL_LENGHT+1)
		{
			stan =  1;
			j = 0;
		}
	}
	else
	{
		SetPixelColor(LedTable, 0x00, 0x00, 0x00, PIXEL_LENGHT-j);
		j++;
		if(j == PIXEL_LENGHT+1)
		{
			stan =  0;
			j = 0;
		}
	}
}

void SnowFlashes(uint8_t *LedTable)
{
	for(i=0; i<PIXEL_LENGHT; i++)
		SetPixelColor(LedTable, 0x07, 0x07, 0x07, i+1);

	k = random()%16;

	if(stan)
	{
		czas = 10;
		SetPixelColor(LedTable, 0xff, 0xff, 0xff, k);
		stan = 0;
	}
	else
	{
		czas = 500;
		stan = 1;
	}
}

void RGBRotary(uint8_t *LedTable)
{
	static uint16_t j=3, k=1;

	if(j == 3)
	{
		SetOnlyOnePixelColor(LedTable, 0x0f, 0x00, 0x00, k, PIXEL_LENGHT);
	}
	else if(j == 2)
	{
		SetOnlyOnePixelColor(LedTable, 0x00, 0x0f, 0x00, k, PIXEL_LENGHT);
	}
	else if(j == 1)
	{
		SetOnlyOnePixelColor(LedTable, 0x00, 0x00, 0x0f, k, PIXEL_LENGHT);
	}

	k++;
	if(k==17)
	{
		k=1;
		j--;
		if(!j)
			j=3;
	}
}
